$('#login-form').validate();

$('#add-image').validate({
    submitHandler: function(){
        var msg = $('#image-url').val(),
            target = $('#text-send').val(),
            new_msg = target + '<img src="' + msg + '" alt="" /> ';
        var str = {
                type: 'image',
                firstname: $('#user-firstname').val(),
                message: new_msg
            },
            data = JSON.stringify(str);
        $.ajax({
            url: '/chat/saveMessage',
            data: str,
            method: 'post',
            typeData: 'json',
            success: function(d){
                conn.send(data);
                $('#image-url').val('');
                $('#myModalImage').modal('hide');
            }
        });

    }
});

$('#add-url').validate({
    submitHandler: function(){
        var msg = $('#link-url').val(),
            target = $('#text-send').val(),
            new_msg = target + ' <a href="' + msg + '" target="_blank">' + msg + '</a> ';
        var str = {
                type: 'link',
                firstname: $('#user-firstname').val(),
                message: new_msg
            },
            data = JSON.stringify(str);
        $.ajax({
            url: '/chat/saveMessage',
            data: str,
            method: 'post',
            typeData: 'json',
            success: function(d){
                conn.send(data);
                $('#link-url').val('');
                $('#myModalLink').modal('hide');
            }
        });

    }
});

$('#add-file-form').validate({
    submitHandler: function(){
        var formData = new FormData($('#add-file-form')[0]);
        $.ajax({
            url: '/chat/upload',  //Server script to process data
            type: 'POST',
            xhr: function() {  // Custom XMLHttpRequest
                var myXhr = $.ajaxSettings.xhr();
                if(myXhr.upload){ // Check if upload property exists
                    myXhr.upload.addEventListener('progress',progressHandlingFunction, false); // For handling the progress of the upload
                }
                return myXhr;
            },
            //Ajax events
            beforeSend: beforeSendHandler,
            success: completeHandler,
            error: errorHandler,
            // Form data
            data: formData,
            //Options to tell jQuery not to process data or worry about content-type.
            cache: false,
            contentType: false,
            processData: false
        });
        return false;
    }
});

function beforeSendHandler(){
    $('#file-upload').hide();
    $('#loader').show();
    $('#add-file').prop('disabled', true);
    $('.modal-body').addClass('modal-body-loader');
}

function completeHandler(data){
    if(data != 'error'){
        var obj = JSON.parse(data);

        var new_msg = '<a href="' + obj.url + '" target="_blank"><b>' + obj.file + '</b> (' + obj.size  + ')</a> ';
        var str = {
                type: 'file',
                firstname: $('#user-firstname').val(),
                message: new_msg
            },
            new_data = JSON.stringify(str);
        $.ajax({
            url: '/chat/saveMessage',
            data: str,
            method: 'post',
            typeData: 'json',
            success: function(d){
                conn.send(new_data);
                clearInputs();
                $('#myModalFile').modal('hide');
            }
        });

    }

    $('#loader').hide();
    $('#file-upload').show();
    $('#add-file').prop('disabled', false);
    $('.modal-body').removeClass('modal-body-loader');

}

function errorHandler(error){
    console.log(error);
}

function progressHandlingFunction(e){
    if(e.lengthComputable){
        $('progress').attr({value:e.loaded,max:e.total});
    }
}

function clearInputs(){
    $('#link-url').val('');
    $('#image-url').val('');
    $('#file-upload').val('');
}