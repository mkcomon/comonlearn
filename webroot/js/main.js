$('#send').click(function(event){
    event.preventDefault();
    var str = {
            type: 'message',
            firstname: $('#user-firstname').val(),
            message: $('#text-send').val()
        },
        data = JSON.stringify(str);
    if($('#text-send').val() != '' && $('#text-send').val() != ' '){
        $.ajax({
            url: '/chat/saveMessage',
            data: str,
            method: 'post',
            typeData: 'json',
            success: function(d){
                conn.send(data);
                $('#text-send').val('');
            }
        });

    }
});