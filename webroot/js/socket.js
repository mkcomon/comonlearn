var conn = new WebSocket('ws://95.85.51.252:3000');

conn.onopen = function(e) {
    console.log("Connection established!");
    var string = {
            type: 'joined',
            firstname: $('#user-firstname').val(),
            message: 'Użytkownik dołączył do chatu!'
        },
        data_to_send = JSON.stringify(string);
    $.get('/chat/getMessages', function(d){
        var obj = JSON.parse(d),
            str = "";
        $.each(obj, function(key, value){
            if(value.message[0] == 'h' && value.message[1] == 't' && value.message[2] == 't' && value.message[3] == 'p' ||
                value.message[0] == 'w' && value.message[1] == 'w' && value.message[2] == 'w'){
                str = '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><b>' + value.firstname + '</b>: <a href="' + value.message + '" target="_blank">' + value.message + '</a></div>';
            }else{
                str = '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><b>' + value.firstname + '</b>: ' + value.message + '</div>';
            }
            $('#messages').append(str);
            str = '';
        });
        conn.send(data_to_send);
    });
};

conn.onmessage = function(e) {
    var obj = JSON.parse(e.data);

    if(obj.message[0] == 'h' && obj.message[1] == 't' && obj.message[2] == 't' && obj.message[3] == 'p' ||
        obj.message[0] == 'w' && obj.message[1] == 'w' && obj.message[2] == 'w'){
        $('#messages').prepend('<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><b>' + obj.firstname + '</b>: <a href="' + obj.message + '" target="_blank">' + obj.message + '</a></div>');
    }else{
        $('#messages').prepend('<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><b>' + obj.firstname + '</b>: ' + obj.message + '</div>');
    }
};



