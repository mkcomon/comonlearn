<div class="alert alert-warning alert-block">
   <button type="button" class="close" data-dismiss="alert">
      <i class="fa fa-times"></i>
   </button>
   <h4>
      <i class="icon-bell-alt"></i><?php echo __('Uwaga!'); ?></h4>
   <p><?php echo $message; ?></p>
</div>