<div class="alert alert-success">
   <button type="button" class="close" data-dismiss="alert">
      <i class="fa fa-times"></i>
   </button>
   <i class="icon-ok-sign icon-large"></i>
   <?php echo $message; ?>
</div>