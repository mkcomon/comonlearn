<div class="alert alert-info">
   <button type="button" class="close" data-dismiss="alert">
      <i class="fa fa-times"></i>
   </button>
   <i class="icon-info-sign icon-large"></i>
   <strong>Informacja:</strong> <?php echo $message; ?>
</div>