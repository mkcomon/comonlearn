<div class="alert alert-danger">
   <button type="button" class="close" data-dismiss="alert">
      <i class="fa fa-times"></i>
   </button>
   <i class="icon-ban-circle icon-large"></i>
   <strong>Błąd!</strong> <?php echo $message; ?>
</div>