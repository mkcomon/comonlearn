<div id="content-login" class="col-lg-4 col-md-7 col-sm-7 col-xs-11">
    <div class="header"><b>//COMON</b> login</div>
    <div class="content">
        <?= $this->Flash->render() ?>
        <?= $this->Flash->render('auth') ?>
        <form method="POST" action="/users/login" id="login-form">
            <div>
                <input type="email" name="email" class="form-control" placeholder="E-MAIL" required/>
            </div>
            <div>
                <input type="password" name="password" class="form-control" placeholder="PASSWORD" required/>
            </div>
            <div style="margin-top: 20px;">
                <input type="submit" class="btn btn-default" value="LOG IN"/>
            </div>
        </form>
    </div>
    <div class="footer">
        Copyright &copy; <?php echo date('Y'); ?><br>
        Made with <b>LOVE</b> by <a href="http://comon.pl/" target="_blank">//COMON</a>
    </div>
</div>