<style>
    .modal-body-loader {
        position: relative;
        height: 150px !important;
    }

    #loader {
        animation: animate 1.5s linear infinite;
        clip: rect(0, 80px, 80px, 40px);
        height: 80px;
        width: 80px;
        position: absolute;
        left: calc(50% - 40px);
        top: calc(50% - 40px);
        display: none;
    }
    @keyframes animate {
        0% {
            transform: rotate(0deg)
        }
        100% {
            transform: rotate(220deg)
        }
    }
    #loader:after {
        animation: animate2 1.5s ease-in-out infinite;
        clip: rect(0, 80px, 80px, 40px);
        content:'';
        border-radius: 50%;
        height: 80px;
        width: 80px;
        position: absolute;
    }
    @keyframes animate2 {
        0% {
            box-shadow: inset #000000 0 0 0 17px;
            transform: rotate(-140deg);
        }
        50% {
            box-shadow: inset #000000 0 0 0 2px;
        }
        100% {
            box-shadow: inset #000000 0 0 0 17px;
            transform: rotate(140deg);
        }
    }
</style>

<input type="hidden" value="<?php $session = $this->request->session(); $name = $session->read('Auth.User.firstname'); echo $name; ?>" id="user-firstname"/>

<!-- Modal Image -->
<div class="modal fade" id="myModalImage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="add-image">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Dodaj obrazek</h4>
                </div>
                <div class="modal-body">
                    <input type="url" id="image-url" name="url" class="form-control" placeholder="URL" required/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" onclick="clearInputs();">Anuluj</button>
                    <button type="submit" class="btn btn-success" id="add-image">Dodaj</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End Modal Image-->
<!-- Modal Link -->
<div class="modal fade" id="myModalLink" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="add-url">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Dodaj link</h4>
                </div>
                <div class="modal-body">
                    <input type="url" id="link-url" name="url" class="form-control" placeholder="Podaj link..." required/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" onclick="clearInputs();">Anuluj</button>
                    <button type="submit" class="btn btn-success" id="add-link">Dodaj</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End Modal Link-->
<!-- Modal File -->
<div class="modal fade" id="myModalFile" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="/chat/upload" enctype="multipart/form-data" id="add-file-form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Dodaj plik</h4>
                </div>
                <div class="modal-body" style="height: 110px;">
                    <input type="file" name="file" id="file-upload" class="form-control parsley-validated" style="padding-top: 8px;" required />
                    <div id="loader"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" onclick="clearInputs();">Anuluj</button>
                    <button type="submit" class="btn btn-success" id="add-file">Dodaj</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End Modal File-->

<div id="content" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id="inputs-section">
        <div style="margin-top: 20px;">
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModalImage">Obrazek</button>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModalLink">Link</button>
            <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#myModalFile">Plik</button>
        </div>
        <form id="send-message">
            <textarea id="text-send" class="form-control" placeholder="Treść wiadomości..." rows="5"></textarea>
            <input class="btn btn-info" type="submit" id="send" value="Wyślij"/>
        </form>
        <a href="/users/logout" class="btn btn-danger" id="logout-button">Wyloguj</a>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id="messages-section">
        <div id="messages" class="col-lg-12 col-md-12 col-sm-12 col-xs-12"></div>
    </div>
</div>