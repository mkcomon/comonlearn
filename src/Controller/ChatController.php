<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
/**
 * Static content controller
 *
 * This controller will render views from Template/Users/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class ChatController extends AppController
{

    public function index()
    {
//        $this->loadModel('Messages');
//        $message = $this->Messages->find()->order(['created_at' => 'desc'])->all();
//        foreach($message as $key => $value){
//            var_dump($value['id']);
//        }
    }

    public function saveMessage(){
        if ($this->request->is('post')) {
            $messagesTable = TableRegistry::get('Messages');
            $message = $messagesTable->newEntity();
            $message->user_id = $this->Auth->user('id');
            $message->message = $this->request->data['message'];
            $message->date = date('d-m-Y H:i:s');

            if($messagesTable->save($message)){
                echo 'true';
            }
        }
    }

    public function getMessages(){
        if ($this->request->is('get')) {
            $this->loadModel('Messages');
            $this->loadModel('Users');
            $messages = $this->Messages->find()->order(['created_at' => 'desc'])->limit(50);
            foreach($messages as $key => $value){
                $user = $this->Users->find()->where(['id' => $value['user_id']])->first();
                $value['firstname'] = $user['firstname'];
            }
            echo json_encode($messages);
        }
    }

    public function upload(){
        if ($this->request->is('post')) {
//            var_dump($this->request->data);
            /*
                file => [
                    name,
                    type,
                    tmp_name,
                    error,
                    size
                ]
            */
            $date = date('Y-m-d H:i:s');
            $dateTimestamp = strtotime($date);
            $file = $this->request->data['file']['name'];
            $exp = explode('.', $file);
            $filename = 'file_' . $dateTimestamp . '.' . $exp[1];
            $new_file = [
                'file' => $file,
                'size' => $this->formatSizeUnits($this->request->data['file']['size']),
                'filename' => $filename,
                'url' => '/uploads/' . $filename
            ];
            if(is_uploaded_file($this->request->data['file']['tmp_name'])){
                move_uploaded_file($this->request->data['file']['tmp_name'],
                    WWW_ROOT . 'uploads' . DS . $filename);
                echo json_encode($new_file);
            }else{
                echo 'error';
            }
        }
    }

    public function connect()
    {

    }

    function formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;
    }
}
